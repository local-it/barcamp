---
author:
  name: "yksflip"
date: 2022-08-12
publishdate: 2022-08-01
draft: false
linktitle: day0
type:
  - post
  - posts
title: Fr. Tag 0
eventname: do.IT.local Barcamp
summary:
eventlocation: Mechow, Germany
weight: 10
---

|       |     |       |     |                                                     |
| ----- | --- | ----- | --- | --------------------------------------------------- |
| 09:00 | --- | 10:00 | :   | Frühstück                                           |
| 10:00 | --- | 10:30 | :   | Checkin und Aufteilen der Slots                     |
| 10:30 | --- | 12:30 | :   | 1. Slot                                             |
| 12:30 | --- | 13:00 | :   | Gemeinsamer Austausch                               |
| 13:00 | --- | 15:00 | :   | Mittagessen                                         |
| 15:00 | --- | 16:30 | :   | 2. Slot                                             |
| 17:00 | --- | 18:30 | :   | 3. Slot                                             |
| 18:30 | --- | 19:00 | :   | Gemeinsamer Austausch, Check-Out                    |
| 19:00 | --- | 20:00 | :   | Abendessen                                          |
| 20:00 | --- | 21:00 | :   | Offizielle Begrüßung und Vorstellung local-it e.V.. |
