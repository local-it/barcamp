---
author:
  name: "yksflip"
date: 2022-08-11
publishdate: 2022-08-01
draft: false
linktitle: day-1
type:
  - post
  - posts
title: Do. Tag -1
eventname: do.IT.local Barcamp
summary: Vorprogramm
eventlocation: Mechow, Germany
weight: 10
---

Gemütliches starten in kleinerer etwas informellerer Runde. 

|       |     |       |     |                                  |
| ----- | --- | ----- | --- | -------------------------------- |
| 09:00 | --- | 10:00 | :   | Frühstück                        |
| 10:00 | --- | 10:30 | :   | Checkin und Aufteilen der Slots  |
| 10:30 | --- | 12:30 | :   | 1. Slot                          |
| 12:30 | --- | 13:00 | :   | Gemeinsamer Austausch            |
| 13:00 | --- | 15:00 | :   | Mittagessen                      |
| 15:00 | --- | 16:30 | :   | 2. Slot                          |
| 17:00 | --- | 18:30 | :   | 3. Slot                          |
| 18:30 | --- | 19:00 | :   | Gemeinsamer Austausch, Check-Out |
| 19:00 | --- | 20:00 | :   | Abendessen                       |
