---
author:
  name: "yksflip"
date: 2022-08-14
publishdate: 2022-08-01
draft: false
linktitle: day2
type:
  - post
  - posts
title: So. Tag 2
eventname: do.IT.local Barcamp
summary:
eventlocation: Mechow, Germany
weight: 10
---

|       |     |       |     |                                 |
| ----- | --- | ----- | --- | ------------------------------- |
| 09:00 | --- | 10:00 | :   | Frühstück                       |
| 10:00 | --- | 10:30 | :   | Checkin und Aufteilen der Slots |
| 10:30 | --- | 12:30 | :   | 1. Slot                         |
| 12:30 | --- | 13:00 | :   | Gemeinsamer Austausch           |
| 13:00 | --- | 15:00 | :   | Mittagessen                     |
| 15:00 | --- | 17:00 | :   | Gemeinsamer Abschluss           |

(ggf auch schon früher)